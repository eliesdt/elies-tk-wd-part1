README IS THE SAME FOR BOTH PARTS OF THE TASK

Part 2 contains pretty much the same files as Part 1 plus:
- A JS file specifically for the API information retrieval
- An additional section in the HTML and SCSS files used to present the information retrieved via the API

# Tools used

The task has been implemented in HTML, SCSS and JS using Visual Studio Code.

Part 2 required the usage of an API to retrieve information about authors and their books. In order to avoid issues with CORS (Cross Origin Resource Sharing) when running the script in local, the calls are made via http://cors-anywhere.herokuapp.com/. However, this might lead to a "Too Many Requests" response sometimes.

# Page structure and naming conventions

Besides a header containing TravelPerk's logo and the footer with the copyright information and some relevant links, the main area of the page is structured as a container composed by the different sections of the landing page. The class names for each of these are:

- main
- perks
- testimonials
- companies
- picture
- authors (Part 2)

The name of the subclasses within each of these sections start with their containing section name.

# Future improvements

If I would have been able to spend more time on the task, I would have tried to implement the following:

- Improve the overall responsiveness of the page. The desktop view and the mobile view are quite accurate regarding the Figma design. However, the transition from one view to the other is not ideal (i.e. if the screen width is 900px the layout is a bit ugly). Also, some of the values used in the CSS should preferably be relative to the screen size instead of them being assigned to static values in px. It's the case of the section with the company logos, which might overflow their respective "boxes" depending on the screen size.

- In the main section of the page, where it says "As used by", in the Figma design of the mobile version, the logos are evenly-spaced in three different rows. The first two rows contain three logos but the last row just two, which then become aligned to the middle. I haven't been achieve to achieve this result, as using the "space-between" value for the "justify-content" property makes each of the logos to be located at the beginning and at the end of the line respectively. I have tried different approaches but none of them has been completely successful. Setting the "justify-content" to "space-evenly" in the mobile view is close to the desired layout but not completely.

- Make a cooler section for Part 2. It's funny cause precisely this week I came across a ["3D Book Image CSS Generator"](https://3d-book-css.netlify.app/) that I thought could look cool in the section required in Part 2 with the covers of the books by each author. However, I have had some struggles with the API information retrieval (CORS problems, Too Many Request Responses, etc.) and I haven't seen it feasible to properly implement it.

# General thoughts

Working on this task has been fun :) I admit I have more experience with the backend, but I'm looking forward to improving my frontend capabilities and I consider I have learned quite a lot with this challenge. I estimate it has taken me between 10 and 12 hours to complete this task, but I'm satisfied with the outcome, and I'm confident that if I had to work on a similar task again, I would be significantly more efficient. I assume I still have a lot to learn, and I guess there are best practices principles and conventions I am not yet familiar with, but I'm willing to get better on it.